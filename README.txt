INSTALLATION INSTRUCTIONS


1. Upload this module into your sites/all/modules or sites/SITENAME/modules directory on your Drupal site.

2. Enable the module using the "Modules" admin page on your site

3. Visit the module's configuration page. (http://www.YOURSITE.com/admin/config/system/freshmarketer)

4. Make sure that "Enabled" is set to "Yes".

5. Enter your API Key into the Freshmarketer API Key field.  Your API Key is available on the Setup->Integrations page in your Freshmarketer account.

6. Save your settings.

That's it!  Enjoy using Crazy Egg on your Drupal website!