<?php

function freshmarketer_admin_settings_form() {
  $form = array();

  validateExperimentCreate();

  $form['freshmarketer_enabled'] = array(
      '#type' => 'radios',
      '#title' => t('Freshmarketer Enabled?'),
      '#options' => array(
          TRUE => t('Yes'),
          FALSE => t('No'),
      ),
      '#default_value' => variable_get('freshmarketer_enabled', TRUE),
  );

  $form['freshmarketer_api_key'] = array(
      '#type' => 'textfield',
      '#required' => 'true',
      '#title' => t('Freshmarketer API Key'),
      '#default_value' => variable_get('freshmarketer_api_key', ''),
      '#description' => '(Paste your Freshmarketer API Key here.)',
  );

  $account_explanation = '';
  $account_explanation .= t("<br />1) login to your <a href='https://app.freshmarketer.com/' target='_blank'>Freshmarketer</a> account & navigate to 'Integrations' under Setup. <br />2) Enable Drupal Integrations.Now, click Generate API Key button in the popup that appears and copy the API Key <br />3) Paste the API key here and start using Freshmarketer. 
<br /><br />Interested in using Freshmarketer? <a href='https://freshmarketer.com/' target='_blank'>click here</a> to signup.");
  $account_explanation .= '<br />';
  $account_explanation .= t('');
  $account_explanation .= '<br />';

  $form['account_explanation'] = array(
      '#type' => 'item',
      '#title' => t('How to use Freshmarketer Plugin?'),
      '#markup' => $account_explanation,
  );

  return system_settings_form($form);
}
function validateExperimentCreate(){
  
$freshmarketer_api_key = variable_get('freshmarketer_api_key', '');
  $freshmarketer_enabled = variable_get('freshmarketer_enabled', '');

  if ($freshmarketer_api_key) {
    if($freshmarketer_enabled){
    $decodedString = base64_decode($freshmarketer_api_key);
    if (!strpos($decodedString, "#")) {
          drupal_set_message(t('Oops. The token seems to be invalid.'),'error',FALSE);
          return false;
    }
    $arrayOfStrings = explode('#',$decodedString);
    $accessToken = $arrayOfStrings[0];
    $orgId = $arrayOfStrings[1];
    $site_url = 'http://' . $_SERVER['HTTP_HOST'].'/';
    $urls = 'http://app.freshmarketer.com/ab/api/org/'.$orgId.'/drupal/token/validate?site_url='.$site_url;
    $postData = '';
    $ch = curl_init();  
    curl_setopt($ch, CURLOPT_URL, $urls);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
    $headers = [
      '_at: '.$accessToken,
      'apirequest: true'
      ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $output=curl_exec($ch);
    $arr = array();
    $arr = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $res = json_decode($output);


    if($arr==200){
      if($res->validate->exp==false){
           drupal_set_message(t('You have created no experiments so far. Please <b><a href="http://app.freshmarketer.com/ab" target="_blank">click here</a></b> to create a new experiment.'),'warning',FALSE);
      }
    }
    if ($arr==500 || $arr==401) {
      drupal_get_messages('status');
      drupal_set_message(t('Oops. The token seems to be invalid.'),'error',FALSE);
      return false;
    }
    curl_close($ch);
    drupal_get_messages('error');
    drupal_get_messages('warning');
  }
}
drupal_get_messages('error');
    drupal_get_messages('warning');
}